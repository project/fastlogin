
Fast login

The module provides tokens with login ticket, which can be used in emails for automatic login of users via links from emails.
Admins can build different tokens for different purposes.

Uses login_ticket module and token module.
Can be used with notifications module and managed_newsletters module.


To install, place the entire module folder into your modules directory.
Go to administer -> site building -> modules and enable the Fast Login module.

To create a new fast login token go to 
Administer -> site Building -> Fast Login Tokens

To enable fast login in your newsletters, add created token to the end of links in email body.
For example:
<p><a href="[node-url][fastlogin-link]">[title]</a></p>
<p>[node-teaser]</p>
